module github.com/tinrab/meower-master

require (
	github.com/99designs/gqlgen v0.5.0 // indirect
	github.com/agnivade/levenshtein v1.0.0 // indirect
	github.com/elastic/go-elasticsearch/v6 v6.8.6-0.20191210132626-f0c66f50f9e7 // indirect
	github.com/elastic/go-elasticsearch/v7 v7.5.1-0.20191210132627-ed288cdc5128 // indirect
	github.com/golang/protobuf v1.2.0 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/websocket v1.4.0
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/lib/pq v1.0.0 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/nkeys v0.1.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/segmentio/ksuid v1.0.1 // indirect
	//github.com/tinrab/meower v0.0.0-20180527150729-a817049dd1e2
	//github.com/tinrab/retry v1.0.0
	github.com/urfave/cli v1.20.0 // indirect
	github.com/vektah/gqlparser v0.0.0-20180831041411-14e83ae06ec1 // indirect
	go.mongodb.org/mongo-driver v1.2.0 // indirect
	golang.org/x/tools v0.0.0-20180904205237-0aa4b8830f48 // indirect
	google.golang.org/genproto v0.0.0-20180831171423-11092d34479b // indirect
	google.golang.org/grpc v1.14.0 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
