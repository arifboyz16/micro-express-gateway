Flask==1.0.2
Werkzeug==0.14.1
flask_restful
elasticsearch==7.1.0
python-dotenv==0.10.3
