"""Run Project."""
import os
from flask import Flask, request, jsonify
from elasticsearch import Elasticsearch
from dotenv import load_dotenv
load_dotenv()

ELASTICSEARCH_HOST = os.getenv("ELASTICSEARCH_HOST")
ELASTICSEARCH_PORT = os.getenv("ELASTICSEARCH_PORT")
ELASTICSEARCH_HTTP_COMPRESS = os.getenv("ELASTICSEARCH_HTTP_COMPRESS")
PORT_APP = os.getenv("PORT_APP")

elastic = Elasticsearch(ELASTICSEARCH_HOST, port=ELASTICSEARCH_PORT, http_compress=True)

app = Flask(__name__)

@app.route('/api/v1/search/', methods=['GET'])
def search():
    elastic_index = request.args.get('index')
    search = request.args.get('search')
    queryset = []
    query = {"match_all" : {}}
    if search:
        query_param = {
            "bool": {
                "must": {
                    "query_string": {
                        "query": "{}".format(search),
                        "fields" : ["*"],
                    }
                }
            }
        }

        query = query_param

    if elastic_index:
        queryset = elastic.search(
            index=elastic_index,
            body={
                "from": 0,
                "size": 25,
                "query": query,
            }
        )
        queryset = queryset['hits']['hits']
    return jsonify(queryset)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=PORT_APP)
