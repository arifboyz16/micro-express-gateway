import os, json, uuid, pymongo
from elasticsearch import Elasticsearch
from dotenv import load_dotenv
load_dotenv()

ELASTICSEARCH_HOST = os.getenv("ELASTICSEARCH_HOST")
ELASTICSEARCH_PORT = os.getenv("ELASTICSEARCH_PORT")
ELASTICSEARCH_HTTP_COMPRESS = os.getenv("ELASTICSEARCH_HTTP_COMPRESS")

elastic = Elasticsearch(ELASTICSEARCH_HOST, port=ELASTICSEARCH_PORT, http_compress=True)

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["booksManagement"]
mycol = mydb["books"]



with open('books.json') as json_file:
    data = json.load(json_file)
    for p in data:
        _id = uuid.uuid4()
        title = p['title']
        author = p['authors'][0]
        numberPages = p['pageCount']
        publisher = p['authors'][0]

        mydict = {
            "_id": str(_id),
            "title": title,
            "author": author,
            "numberPages": numberPages,
            "publisher": publisher
            }
        x = mycol.insert_one(mydict)
        if x:
            del mydict["_id"]
            elastic.index(
                refresh=True,
                index="books",
                doc_type="_doc",
                id=str(_id),
                body=mydict)
