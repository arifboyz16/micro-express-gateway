import asyncio, os, json
from elasticsearch import Elasticsearch
from dotenv import load_dotenv
load_dotenv()

ELASTICSEARCH_HOST = os.getenv("ELASTICSEARCH_HOST")
ELASTICSEARCH_PORT = os.getenv("ELASTICSEARCH_PORT")
ELASTICSEARCH_HTTP_COMPRESS = os.getenv("ELASTICSEARCH_HTTP_COMPRESS")

elastic = Elasticsearch(ELASTICSEARCH_HOST, port=ELASTICSEARCH_PORT, http_compress=True)

async def createIndex(nats, reply, data):
    """Method to create elasticsearch index without id."""
    parseData = json.loads(data.decode('utf8'))
    response = elastic.index(
        refresh=True,
        index=parseData["name_index"],
        doc_type="_doc",
        body=parseData["data"])
    await nats.publish(reply, json.dumps(response).encode('utf8'))

async def createIndexWithID(nats, reply, data):
    """Method to create elasticsearch index with id."""
    parseData = json.loads(data.decode('utf8'))
    response = elastic.index(
        refresh=True,
        index=parseData["name_index"],
        doc_type="_doc",
        id=parseData["_id"],
        body=parseData["data"])
    await nats.publish(reply, json.dumps(response).encode('utf8'))

async def updateIndexByID(nats, reply, data):
    """Method to update elasticsearch index by ID."""
    parseData = json.loads(data.decode('utf8'))
    response = elastic.update(
        refresh=True,
        index=parseData["name_index"],
        id=parseData["_id"],
        body={"doc": parseData["data"]})
    await nats.publish(reply, json.dumps(response).encode('utf8'))

async def updateIndexByQuery(nats, reply, data):
    """Method to update elasticsearch index by Query."""
    parseData = json.loads(data.decode('utf8'))
    if parseData["_id"] is not None:
        query = {
            "match_phrase": parseData["query"]
        }
        getId = elastic.search(index=parseData["name_index"],body={"query": query})

        if getId:
            response = elastic.update(
                refresh=True,
                index=parseData["name_index"],
                id=str(getId['hits']['hits'][0]['_id']),
                body={"doc": parseData["data"]})
            await nats.publish(reply, json.dumps(response).encode('utf8'))
        else:
            await nats.publish(reply, b"Data Not Found")
    else:
        await nats.publish(reply, b"Data Not Found")

async def deleteIndexByID(nats, reply, data):
    """Method to delete elasticsearch index by ID."""
    parseData = json.loads(data.decode('utf8'))
    response = elastic.delete(
        index=parseData["name_index"],
        id=parseData["_id"],
        refresh=True)
    await nats.publish(reply, json.dumps(response).encode('utf8'))

async def deleteIndexByQuery(nats, reply, data):
    """Method to delete elasticsearch index by Query."""
    parseData = json.loads(data.decode('utf8'))
    if parseData["_id"] is not None:
        query = {
            "match_phrase": parseData["query"]
        }
        response = elastic.delete_by_query(
            refresh=True,
            index=parseData["name_index"],
            body={"query": query})
        await nats.publish(reply, json.dumps(response).encode('utf8'))
    else:
        await nats.publish(reply, b"Data Not Found")
