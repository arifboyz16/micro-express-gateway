import asyncio, json
from datetime import datetime
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers

async def run(loop):
    nc = NATS()

    await nc.connect("nats://127.0.0.1:4222", loop=loop)

    async def request_handler(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data.decode()
        print("Received a message on '{subject} {reply}': {data}".format(
            subject=subject, reply=reply, data=data))

    # Signal the server to stop sending messages after we got 10 already.
    if nc.is_connected:
        # Subscription using a 'workers' queue so that only a single subscriber
        # gets a request at a time.
        try:
            createData = {
              "name_index": "example",
              "_id": "f05c3d0e-4ea8-4b45-9f3c-a7dcc3f98139",
              "data": {
                "city": "New York",
                "post_code": 1
              }
            }
            updateData = {
              "name_index": "example",
              "id": 2,
              "data": {
                "id": 1,
                "city": "New York",
                "post_code": 1
              }
            }
            deleteData = {
              "name_index": "books",
              "_id": "50f36246-dc9e-4014-8068-4e0b4330d95a",
            }
            response = await nc.request("indexing.deleteByID", json.dumps(createData).encode('utf8'), expected=10, cb=request_handler)
            # that sent messages have been processed already.
            await nc.flush(0.500)
        except ErrTimeout:
            print("[Error] Timeout!")

        # Wait a bit for messages to be dispatched...
        await asyncio.sleep(2, loop=loop)

        # Detach from the server.
        await nc.close()

    if nc.last_error is not None:
        print("Last Error: {}".format(nc.last_error))

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop))
    loop.close()
