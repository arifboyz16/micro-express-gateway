import asyncio, os, signal
from dotenv import load_dotenv
from nats.aio.client import Client as NATS
from indexingElastic import createIndex, createIndexWithID, updateIndexByID, updateIndexByQuery, deleteIndexByID, deleteIndexByQuery
load_dotenv()

NATS_HOST = os.getenv("NATS_HOST")

async def main(loop):
    nc = NATS()

    async def closed_cb():
        print("Connection to NATS is closed.")
        await asyncio.sleep(0.3, loop=loop)
        loop.stop()

    options = {
        "servers": [NATS_HOST],
        "io_loop": loop,
        "closed_cb": closed_cb
    }

    await nc.connect(**options)
    print("Connected to NATS at {}...".format(nc.connected_url.netloc))

    async def subscribe_indexing(msg):
        subject = msg.subject
        reply = msg.reply
        data = msg.data
        if (subject == 'indexing.create'):
            await createIndex(nc, reply, data)

        elif (subject == 'indexing.createWithID'):
            await createIndexWithID(nc, reply, data)

        elif (subject == 'indexing.updateByID'):
            await updateIndexByID(nc, reply, data)

        elif (subject == 'indexing.updateByQuery'):
            await updateIndexByQuery(nc, reply, data)

        elif (subject == 'indexing.deleteByID'):
            await deleteIndexByID(nc, reply, data)

        elif (subject == 'indexing.deleteByQuery'):
            await deleteIndexByQuery(nc, reply, data)

        else:
            await nats.publish(reply, b"Function not found")

    # one subscriber handles message a request at a time.
    await nc.subscribe("indexing.>", "indexing", subscribe_indexing)

    def signal_handler():
        if nc.is_closed:
            return
        print("Disconnecting...")
        loop.create_task(nc.close())

    for sig in ('SIGINT', 'SIGTERM'):
        loop.add_signal_handler(getattr(signal, sig), signal_handler)

    # Gracefully close the connection.
    # await nc.drain()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    try:
        loop.run_forever()
    finally:
        loop.close()
