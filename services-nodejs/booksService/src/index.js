const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const express = require("express")
const helmet = require('helmet')
const http = require('http')
const path = require('path')
const cors = require('cors')
const Raven = require('raven')
import logger from './utils/logger';

const env = process.env.NODE_ENV
try {
  switch(env) {
    case 'undefined':
      require('dotenv').config();
      break
    case 'development':
      require('dotenv').config({
        path: path.resolve(process.cwd(), '../.env'),
      })
      break
    default:
      Error('Unrecognized Environment')
  }
} catch (err) {
  Error('Error trying to run file')
}

Raven.config(process.env.SENTRY_DSN).install();
const db = require("./utils/database").mongoURI
const indexingEvent = require('./event/indexingEvent')

const app = express()

app.use(cors())
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const connectWithRetry = function() {
  return mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true }, function(err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
      setTimeout(connectWithRetry, 5000);
    } else {
      console.log("mongoDB Connected")
    }
  });
};
connectWithRetry();

mongoose.Promise = global.Promise

app.set('models', mongoose.models)

// Import modules
const route = require('./routes')

//routes
app.use('/api/v1/books', route)

// The request handler must be the first middleware on the app
app.use(Raven.requestHandler());

// The error handler must be before any other error middleware
app.use(Raven.errorHandler());

app.all('*', (request, response) => response.status(404).json({
  status: 'fail',
  message: 'Route-unavailable',
}));

app.use((error, request, response) => {
  logger.log({
    level: 'error',
    message: error.message,
  });
  return response.status(500).json({
    status: 'error',
    message: 'Internal Server error',
  });
});

// Create http server
// const server = http.createServer(app)

const port = process.env.BOOKS_MANAGEMENT_SERVICE_PORT || 3003;

app.listen(port, () => logger.log({
  level: 'info',
  message: `'Books service listening on port ' ${port}`,
}));
