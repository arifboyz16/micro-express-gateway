const servers_nats = [process.env.NATS_URI]
const nats = require('nats').connect({'servers': servers_nats})
const { errors, APIError } = require('../utils/exceptions')

async function createIndexWithID(id, data) {
  const responseData = []
  try {
    const createData = {
      "name_index": "books",
      "_id": id,
      "data": data
    }
    const sid = await nats.request('indexing.createWithID', JSON.stringify(createData), (response) => {
      responseData.push(JSON.parse(response))
    })

    setTimeout(() => {
      nats.unsubscribe(sid)
      return responseData
    }, 1000)
  } catch (error) {
    return errors.serverError
  }
}

async function updateIndex(id, data) {
  const responseData = []
  try {
    const updateData = {
      "name_index": "books",
      "_id": id,
      "data": data
    }

    const sid = await nats.request('indexing.updateByID', JSON.stringify(updateData), (response) => {
      responseData.push(JSON.parse(response))
    })

    setTimeout(() => {
      nats.unsubscribe(sid)
      return responseData
    }, 200)
  } catch (error) {
    return errors.serverError
  }
}

async function deleteIndex(id) {
  const responseData = []
  try {
    const deleteData = {
      "name_index": "books",
      "_id": id
    }
    const sid = await nats.request('indexing.deleteByID', JSON.stringify(deleteData), (response) => {
      responseData.push(JSON.parse(response))
    })

    setTimeout(() => {
      nats.unsubscribe(sid)
      return responseData
    }, 200)
  } catch (error) {
    return errors.serverError
  }
}

module.exports = {
  createIndexWithID,
  updateIndex,
  deleteIndex
}
