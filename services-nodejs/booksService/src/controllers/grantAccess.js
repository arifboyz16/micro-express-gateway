const { errors } = require('../utils/exceptions')
const { roles } = require('../roles')

module.exports = function(action, resource) {
  return async (req, res, next) => {
    try {
      const name_access = req.user.role[0].permission["books"].name_menus
      const check_access = req.user.role[0].permission["books"].access[resource].checked
      const permission = roles.can(name_access)[action](String(check_access))
      if (!permission.granted) {
        return res.status(403).send(errors.forbidden)
      }
      next()
    } catch (error) {
      return res.status(403).send(errors.forbidden)
      next(error)
    }
  }
}
