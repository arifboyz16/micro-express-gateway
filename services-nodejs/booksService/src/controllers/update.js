const { errors } = require('../utils/exceptions')
const { updateIndex } = require('../event/indexingEvent')

// Import Model
const Books = require('../models/Books')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { id: _id } = req.params

    const {
      title = null,
      author = null,
      numberPages = null,
      publisher = null
    } = req.body

    const data = {
      title,
      author,
      numberPages,
      publisher
    }

    await Books.findOneAndUpdate({ _id }, data)
    await updateIndex(_id, data)

    res.status(201).send({
      message: 'Update data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
