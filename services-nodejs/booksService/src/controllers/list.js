const { errors } = require('../utils/exceptions')
// Import model
const Books = require('../models/Books')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    let results

    const { title, author, search} = req.query // eslint-disable-line
    const page = parseInt(req.query.page) - 1 || 0 // eslint-disable-line
    const pageSize = parseInt(req.query.pageSize) || 5 // eslint-disable-line
    const skip = page * pageSize

    const rules = [
      {
        '$project': {
          _id: 0,
          id: '$_id',
          title: 1,
          author: 1,
          numberPages: 1,
          publisher: 1
        },
      },
    ]

    if (search) {
      const terms = new RegExp(search, 'i')

      rules.push({
        '$match': {
          'name': {
            '$regex': terms,
          },
        },
      })
    }

    const countRules = [
      ...rules,
      {
        '$group': { _id: null, rows: { '$sum': 1 } },
      },
      {
        '$project': {
          rows: 1,
        },
      },
    ]

    const total = await Books.aggregate(countRules)

    results = await Books
      .aggregate(rules)
      .sort({ name: 1 })
      .skip(skip)

    res.status(200).send({
      total: total.length > 0 ? total[0].rows : 0,
      page: page + 1,
      pageSize,
      result: results
    })


  } catch (error) {
    console.error(error) // eslint-disable-line
    res.status(500).send(errors.serverError)
  }
}
