const { errors } = require('../utils/exceptions')
const { createIndexWithID } = require('../event/indexingEvent')

// Import Model
const Books = require('../models/Books')

module.exports = async (req, res) => { // eslint-disable-line
  try {

    const {
      title = null,
      author = null,
      numberPages = null,
      publisher = null
    } = req.body

    const data = {
      title,
      author,
      numberPages,
      publisher
    }

    const dataBooks = await Books.create(data)
    await createIndexWithID(dataBooks._id, data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
