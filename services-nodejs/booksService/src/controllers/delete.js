const { errors } = require('../utils/exceptions')
const { deleteIndex } = require('../event/indexingEvent')

// Import Model
const Books = require('../models/Books')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const {
      id: _id,
    } = req.params

    await Books.deleteOne({ _id })
    await deleteIndex(_id)

    res.status(201).send({
      message: 'Delete data successfull',
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
