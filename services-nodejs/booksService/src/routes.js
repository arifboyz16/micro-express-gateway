const express = require('express')
// Import middleware
const authenticate = require('./controllers/authenticate')
const grantAccess = require('./controllers/grantAccess')

const router = express.Router()

// Import methods
const list = require('./controllers/list')
const create = require('./controllers/create')
const update = require('./controllers/update')
const remove = require('./controllers/delete')

router.get('/', authenticate, grantAccess('readAny', 'listBooks'), list)
router.post('/', authenticate, grantAccess('createAny', 'createBooks'), create)
router.put('/:id', authenticate, grantAccess('updateAny', 'updateBooks'), update)
router.delete('/:id', authenticate, grantAccess('deleteAny', 'deleteBooks'), remove)

module.exports = router
