const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const uuidv4 = require('uuid/v4')

const Schema = mongoose.Schema

const Books = new Schema({
  _id: {
    type: String,
    default: uuidv4,
  },
  title: {
      type: String,
      require: true
  },
  author: {
      type: String,
      require: true
  },
  numberPages: {
      type: Number,
      require: false
  },
  publisher: {
      type: String,
      require: false
  }
})

Books.index({ author: 1 })

module.exports = mongoose.models.Books || mongoose.model('Books', Books)
