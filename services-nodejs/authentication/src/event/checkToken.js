const jwt = require('jsonwebtoken')
const servers_nats = [process.env.NATS_URI]
const nats = require('nats').connect({'servers': servers_nats})

const { errors, APIError } = require('../utils/exceptions')

const {
  JWT_SECRET_KEY = 'JWT_SECRET_KEY',
} = process.env;

const sid = nats.subscribe(
    'authentication.checkToken',
    {'queue': 'authentication.checkToken'},
  async function(req, reply) {
    const token = JSON.parse(req)
    const status = []

  await nats.publish(reply, JSON.stringify({status}))
})
