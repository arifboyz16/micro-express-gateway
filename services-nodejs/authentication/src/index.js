const bodyParser = require("body-parser")
const express = require("express")
const helmet = require('helmet')
const http = require('http')
const path = require('path')
const compression = require('compression')
const cors = require('cors')
const Raven = require('raven')
import logger from './utils/logger';

const env = process.env.NODE_ENV
try {
  switch(env) {
    case 'undefined':
      require('dotenv').config();
      break
    case 'development':
      require('dotenv').config({
        path: path.resolve(process.cwd(), '../.env'),
      })
      break
    default:
      Error('Unrecognized Environment')
  }
} catch (err) {
  Error('Error trying to run file')
}

const {
  AUTHENTICATION_SERVICE_PORT = 3001,
} = process.env;

Raven.config(process.env.SENTRY_DSN).install();

const app = express()

app.use(Raven.requestHandler());
app.use(Raven.errorHandler());
app.use(cors());
app.use(compression());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Import modules
const auth = require('./modules/auth')

//routes
app.use('/api/v1/auth', auth)

app.all('*', (request, response) => response.status(404).json({
  status: 'fail',
  message: 'Route-unavailable',
}));

app.use((error, request, response) => {
  logger.log({
    level: 'error',
    message: error.message,
  });
  return response.status(500).json({
    status: 'error',
    message: 'Internal Server error',
  });
});

// Create http server
// const server = http.createServer(app)

const port = AUTHENTICATION_SERVICE_PORT || 5001;

app.listen(port, () => logger.log({
  level: 'info',
  message: `'Authentication service listening on port ' ${port}`,
}));
