const servers_nats = [process.env.NATS_URI]
const nats = require('nats').connect({'servers': servers_nats})
const { errors, APIError } = require('../../../utils/exceptions')

module.exports = async (req, res) => {
  try {
    const { email, password } = req.body
    const data = []
    const sid = await nats.request('user.checkUser', JSON.stringify(req.body), (response) => {
      data.push(JSON.parse(response))
    })

    setTimeout(() => {
      nats.unsubscribe(sid)
      res.send({data})
    }, 200)
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
