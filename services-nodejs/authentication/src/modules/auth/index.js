const express = require('express')

const router = express.Router()

// Import methods
const login = require('./methods/login')

// Auth
router.post('/login', login)

module.exports = router
