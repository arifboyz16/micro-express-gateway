const AccessControl = require("accesscontrol");

exports.roles = (function() {
  const ac = new AccessControl();
  ac.grant("User")
    .createAny('true')
    .readAny('true')
    .updateAny('true')
    .deleteAny('true')
  return ac;
})();
