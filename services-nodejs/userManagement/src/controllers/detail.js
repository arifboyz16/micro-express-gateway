const { errors } = require('../utils/exceptions')

// Import model
const User = require('../models/User')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { id: _id } = req.params

    const data = await User.findOne({ _id }).lean()

    data.id = data._id

    delete data._id

    res.send(data)
  } catch (error) {
    res.status(500).send(errors.serverError)
  }
}
