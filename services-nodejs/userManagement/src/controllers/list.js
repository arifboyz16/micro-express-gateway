const { errors } = require('../utils/exceptions')
// Import model
const User = require('../models/User')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    let results

    const { nama, type_user, search} = req.query // eslint-disable-line
    const page = parseInt(req.query.page) - 1 || 0 // eslint-disable-line
    const pageSize = parseInt(req.query.pageSize) || 5 // eslint-disable-line
    const skip = page * pageSize

    const rules = [
      {
        '$match': {
          is_active: true,
        },
      },
      {
        '$project': {
          _id: 0,
          id: '$_id',
          nip: 1,
          email: 1,
          first_name: 1,
          last_name: 1,
          username: 1,
          handphone: 1,
          created_date: 1,
          name : { $concat : [ "$first_name", " ", "$last_name" ]},
        },
      },
    ]

    if (search) {
      const terms = new RegExp(search, 'i')

      rules.push({
        '$match': {
          'name': {
            '$regex': terms,
          },
        },
      })
    }

    if (nama) {
      const nTerms = new RegExp(nama, 'i')

      rules.push({
        '$match': {
          'name': {
            '$regex': nTerms,
          },
        },
      })
    }

    const countRules = [
      ...rules,
      {
        '$group': { _id: null, rows: { '$sum': 1 } },
      },
      {
        '$project': {
          rows: 1,
        },
      },
    ]

    const total = await User.aggregate(countRules)

    results = await User
      .aggregate(rules)
      .sort({ name: 1 })
      .skip(skip)

    res.status(200).send({
      total: total.length > 0 ? total[0].rows : 0,
      page: page + 1,
      pageSize,
      result: results
    })


  } catch (error) {
    console.error(error) // eslint-disable-line
    res.status(500).send(errors.serverError)
  }
}
