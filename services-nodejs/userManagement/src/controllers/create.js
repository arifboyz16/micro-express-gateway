const { errors } = require('../utils/exceptions')

// Import Model
const User = require('../models/User')

module.exports = async (req, res) => { // eslint-disable-line
  try {

    const {
      nik = null,
      email = null,
      username = null,
      password = null,
      first_name = null,
      last_name = null,
      handphone = null,
      role = null,
      is_active = true
    } = req.body

    const data = {
      nik,
      email,
      username,
      password,
      first_name,
      last_name,
      handphone,
      role,
      is_active
    }

    await User.create(data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
