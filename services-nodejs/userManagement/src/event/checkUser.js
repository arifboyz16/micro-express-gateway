const servers_nats = [process.env.NATS_URI]
const nats = require('nats').connect({'servers': servers_nats})
const jwt = require('jsonwebtoken')

const { errors, APIError } = require('../utils/exceptions')
const User = require('../models/User')

const {
  JWT_SECRET_KEY = 'JWT_SECRET_KEY',
} = process.env;

const sid = nats.subscribe('user.checkUser', {'queue': 'user.checkUser'}, async function(req, reply) {
  const { email, password } = JSON.parse(req)
  const data = []
  try{

    const user = await User.findOne({ email, is_active: true })
    const authenticated = await user.comparePassword(password)
    if (authenticated) {
      const {
        _id,
        email: uEmail,
        nik,
        first_name,
        last_name,
        role
      } = user
      const fullName = `${first_name} ${last_name}`

      const token = await jwt.sign({
        _id,
        email: uEmail,
        nik,
        nama: fullName,
        role
        }, JWT_SECRET_KEY, {
        expiresIn: 6400,
        }
      )

      await data.push({
        token,
        created: new Date(),
        user: {
          id: _id,
          email,
          nama: fullName,
          nik,
          role,
        },
      })
    } else {
      await data.push(errors.wrongCredentials)
    }
  } catch (error) {
    console.log(error)
    await data.push(errors.wrongCredentials)
  }

  await nats.publish(reply, JSON.stringify({data}))
})
