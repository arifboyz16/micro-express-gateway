const express = require('express')
// Import middleware
const authenticate = require('./controllers/authenticate')
const grantAccess = require('./controllers/grantAccess')

const router = express.Router()

// Import methods
const list = require('./controllers/list')
const create = require('./controllers/create')
const update = require('./controllers/update')
const remove = require('./controllers/delete')
const detail = require('./controllers/detail')

router.post('/signup', create)
router.get('/', authenticate, grantAccess('readAny', 'listUser'), list)
router.post('/', authenticate, grantAccess('createAny', 'createUser'), create)
router.put('/:id', authenticate, grantAccess('updateAny', 'updateUser'), update)
router.delete('/:id', authenticate, grantAccess('deleteAny', 'deleteUser'), remove)
router.get('/:id', authenticate, grantAccess('readAny', 'detailUser'), detail)

module.exports = router
