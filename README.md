# micro-service-with-express-gateway

##### Running Mongodb
##### Running Redis
##### Running nats

##### Setup env variables
```bash
cp .env-sample .env
```

##### Install Dependencies
- Run the commands below to install the required `node` dependencies
```bash
npm install yarn -g
yarn install && yarn bootstrap
```

### Usage
- To start up your newly installed **micro-service** run
```sh
yarn start
```

### Or running using docker-compose
- Run the commands
```bash
docker-compose up build
```

### Start/Stop all services

```bash
make start-all
make stop-all
```

### Start/Stop services one by one

#### services-database

* Compose file: [./docker-compose.service-database.yml](./docker-compose.service-database.yml)
* Start: `make start-service-database`
* Stop: `make stop-service-database`

#### services-index-database

* Compose file: [./docker-compose.elasticsearch.yml](./docker-compose.elasticsearch.yml)
* Start: `make start-elastic-kibana`
* Stop: `make stop-elastic-kibana`

#### services-nodejs

* Compose file: [./docker-compose.service-nodejs.yml](./docker-compose.service-nodejs.yml)
* Start: `make start-service-nodejs`
* Stop: `make stop-service-nodejs`

#### services-python

* Compose file: [./docker-compose.service-python.yml](./docker-compose.service-python.yml)
* Start: `make start-service-python`
* Stop: `make stop-service-python`

### Login
- Run the commands
```bash
url: http://localhost:8080/auth/login
body: {
  "email":"admin@example.com",
  "password": "1234567"
}
```

### User List
- Run the commands
```bash
url: http://localhost:8080/user/
response:
{
    "total": 1,
    "page": 1,
    "pageSize": 5,
    "result": [
        {
            "handphone": "08523664365",
            "created_date": "2019-11-02T10:46:59.293Z",
            "email": "admin@example.com",
            "username": "admin",
            "first_name": "Admin",
            "last_name": "Example",
            "id": "4ef2b35e-8b0c-414f-9cb7-fece5f19b863",
            "name": "Admin Example"
        }
    ]
}
```

### Books List
- Run the commands
```bash
url: http://localhost:8080/books/
response:
{
    "total": 1,
    "page": 1,
    "pageSize": 5,
    "result": [
        {
            "title": "coba",
            "author": "coba",
            "numberPages": 100,
            "publisher": "coba",
            "id": "7ce1df36-a943-4788-ad6a-11f88cce22f4"
        }
    ]
}
```
